package harkka1;

/*
 * Tekijä: Joonas Hakkarainen 0438374
 */

public class PostOffice {
	String postOffice;
	String availability;
	String address;
	String code;
	String city;
	
	public PostOffice(String new_postOffice, String new_availability, String new_address, String new_code, String new_city) {
		postOffice = new_postOffice;
		availability = new_availability;
		address = new_address;
		code = new_code;
		city = new_city;
	}
	
	@Override
	public String toString() {
		return postOffice;
	}
	
	public String getOffice() {
		return postOffice;
	}
	
	public String getAvailability() {
		return availability;
	}
	
	public String getAddress() {
		return address;
	}
	
	public String getCode() {
		return code;
	}
	
	public String getCity() {
		return city;
	}
	
	public String getOfficeForMap() {
		return address + ", " + code + ", " + city;
	}
	
	public String getInfoForMap() {
		return postOffice + ", " + availability;
	}
}
