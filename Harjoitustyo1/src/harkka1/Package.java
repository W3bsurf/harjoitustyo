package harkka1;

/*
 * Tekijä: Joonas Hakkarainen 0438374
 */

public class Package {
	int packageID;
	int shippingClass;
	int itemID;
	double routeLength;
	String startOffice;
	String endOffice;
	String itemName;
	
	public Package(int new_packageID, int new_shippingClass, int new_itemID, double new_routeLength, String new_itemName, String new_startOffice, String new_endOffice) {
		packageID = new_packageID;
		shippingClass = new_shippingClass;
		itemID = new_itemID;
		routeLength = new_routeLength;
		itemName = new_itemName;
		startOffice = new_startOffice;
		endOffice = new_endOffice;
	}
	
	@Override
	public String toString() {
		return itemName + " " + shippingClass + ". toimitusluokka";
	}
	
	public int getPackageID() {
		return packageID;
	}
	
	public int getShippingClass() {
		return shippingClass;
	}
	
	public int getItemID() {
		return itemID;
	}
	
	public double getRouteLength() {
		return routeLength;
	}
	
	public String getItemName() {
		return itemName;
	}
	
	public String getStartOffice() {
		return startOffice;
	}
	
	public String getEndOffice() {
		return endOffice;
	}
}
