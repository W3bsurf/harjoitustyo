package harkka1;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.web.WebView;
import javafx.stage.Modality;
import javafx.stage.Stage;

/*
 * Tekijä: Joonas Hakkarainen 0438374
 */

public class FXMLDocumentMapViewController implements Initializable{	
	@FXML
	private WebView webView;	
	
	@FXML
	private TextArea packageInfoArea;
	
	@FXML
	private Label packageInfoLabel;
	
	@FXML
	private Button itemButton;
	
	@FXML
	private Button officeButton;
	
	@FXML
	private Button sendButton;
	
	@FXML
	private Button deletePathsButton;
	
	@FXML
	private Button createPackageButton;
	
	@FXML
	private Button deleteItemButton;
	
	@FXML
	private Button deletePackageButton;
	
	@FXML
	private Button printDeliveriesButton;
	
	@FXML
	private Button openPackageButton;
	
	@FXML
	private Button openItemButton;
	
	@FXML
	private Button logoutButton;
	
	@FXML
	private Button openLogButton;
	
	@FXML
	private ComboBox<Item> itemCombo;
	
	@FXML
	private ComboBox<ShipmentClass> shipmentClassCombo;
	
	@FXML
	private ComboBox<PostOffice> startOfficeCombo;
	
	@FXML
	private ComboBox<PostOffice> endOfficeCombo;
	
	@FXML
	private ComboBox<Package> packageCombo;
	
	@FXML
	private ComboBox<String> startCityCombo;
	
	@FXML
	private ComboBox<String> endCityCombo;
	
	// Kirjautuu ulos ja palaa sisäänkirjautumisnäkymään
	public void logout(ActionEvent event) {
		Stage stage = new Stage();
		Parent page;
		try {
			page = FXMLLoader.load(getClass().getResource("FXMLDocumentLoginView.fxml"));
			Scene scene = new Scene(page);
			stage.setScene(scene);
			stage.setTitle("Tervetuloa");
			scene.getStylesheets().add(Main.class.getResource("application.css").toExternalForm());
			stage.show();
			stage.setResizable(false);
			
			Stage cStage = (Stage)((Node)event.getSource()).getScene().getWindow();
			cStage.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	// Avaa lokinäkymäikkunan
	public void openLog(ActionEvent event) {
		Stage stage = new Stage();
		Parent page;
		try {
			page = FXMLLoader.load(getClass().getResource("FXMLDocumentLogView.fxml"));
			Scene scene = new Scene(page);
			stage.setScene(scene);
			stage.setTitle("Loki");
			stage.initModality(Modality.APPLICATION_MODAL);
			scene.getStylesheets().add(Main.class.getResource("application.css").toExternalForm());
			stage.showAndWait();
			stage.setResizable(false);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/*
	 * Lähettää paketin ja piirtää reitin kartalle
	 * Lisää paketin alku- ja lähtöpisteen kartalle
	 * Tarkistaa hajoaako esine matkalla
	 */
	public void sendPackage(ActionEvent event) {
		if (packageCombo.getValue() != null) {
			int itemStatus = SqlManager.getPostSelect().selectItemStatusCheck(packageCombo.getValue().getItemID(), packageCombo.getValue().getShippingClass());
			ArrayList<String> coordinateList = SqlManager.getPostSelect().selectCoordinates(packageCombo.getValue().getStartOffice(), packageCombo.getValue().getEndOffice());
			String mapStartOfficeInfo= SqlManager.getPostSelect().selectOfficeMap(packageCombo.getValue().getStartOffice());
			String mapStartInfo = packageCombo.getValue().getStartOffice() + ", " + SqlManager.getPostSelect().selectOfficeInfo(packageCombo.getValue().getStartOffice());
			String mapEndOfficeInfo= SqlManager.getPostSelect().selectOfficeMap(packageCombo.getValue().getEndOffice());
			String mapEndInfo = packageCombo.getValue().getEndOffice() + ", " + SqlManager.getPostSelect().selectOfficeInfo(packageCombo.getValue().getEndOffice());
			
			webView.getEngine().executeScript("document.goToLocation('" + mapStartOfficeInfo + "', '" + mapStartInfo + "', 'red')");
			webView.getEngine().executeScript("document.goToLocation('" + mapEndOfficeInfo + "', '" + mapEndInfo + "', 'red')");
			webView.getEngine().executeScript("document.createPath(" + coordinateList + ", 'red','" + packageCombo.getValue().getShippingClass() + "')");
			
			SqlManager.getPostUpdate().updatePackageStatus(ListManager.getPackageList(), ListManager.getPackageSentList(), packageCombo.getValue().getPackageID());
			packageInfoArea.appendText("\nPaketti lähetettiin.");
			if (itemStatus == 1) {
				SqlManager.getPostInsert().insertItemBrokeEvent(packageCombo.getValue().getPackageID(), packageCombo.getValue().getItemID());
				packageInfoArea.appendText("\nPaketin sisältö hajosi matkan aikana.");
			}
			SqlManager.getPostInsert().insertPackageArrived(packageCombo.getValue().getPackageID());
			
			packageCombo.getSelectionModel().clearSelection();
			packageInfoArea.appendText("\nPaketti saapui perille.");
		} else {
			Popups.popup("Huomio", "Paketin lähettäminen ei onnistunut!", "Valitse ensin lähetettävä paketti");
		}
	}
	
	// Poistaa kartalle piirretyt reitit
	public void deletePaths(ActionEvent event) {
		webView.getEngine().executeScript("document.deletePaths()");
	}
	
	// Avaa paketin luomisikkunan
	public void openCreatePackage(ActionEvent event) {
		Stage stage = new Stage();
		Parent page;
		try {
			FXMLLoader fxmlLoader = new FXMLLoader();
			fxmlLoader.setLocation(getClass().getResource("FXMLDocumentPackageView.fxml"));
			page = fxmlLoader.load();
			FXMLDocumentPackageViewController c = (FXMLDocumentPackageViewController) fxmlLoader.getController();
			c.webView = webView;
			Scene scene = new Scene(page);
			stage.setScene(scene);
			stage.setTitle("Luo paketti");
			stage.initModality(Modality.APPLICATION_MODAL);
			scene.getStylesheets().add(Main.class.getResource("application.css").toExternalForm());
			stage.showAndWait();
			stage.setResizable(false);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	// Lisää kartalle valitun pakettiautomaatin
	public void addPostOffice(ActionEvent event) {
		if (startOfficeCombo.getValue() != null) {
			String mapstartOfficeCombo = startOfficeCombo.getValue().getOfficeForMap();
			String mapStartInfo = startOfficeCombo.getValue().getInfoForMap();
			
			webView.getEngine().executeScript("document.goToLocation('" + mapstartOfficeCombo + "', '" + mapStartInfo + "', 'red')");
		} else {
			Popups.popup("Huomio", "Automaatin lisääminen kartalle ei onnistunut!", "Valitse ensin lisättävä automaatti");
		}
	}
	
	// Avaa esineen luomisikkunan
	public void openCreateItem(ActionEvent event) {
		Stage stage = new Stage();
		Parent page;
		try {
			page = FXMLLoader.load(getClass().getResource("FXMLDocumentItemView.fxml"));
			Scene scene = new Scene(page);
			stage.setScene(scene);
			stage.setTitle("Luo esine");
			stage.initModality(Modality.APPLICATION_MODAL);
			scene.getStylesheets().add(Main.class.getResource("application.css").toExternalForm());
			stage.showAndWait();
			stage.setResizable(false);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	// Tulostaa comboBoxissa valitun paketin tiedot textAreaan
	public void printPackageInfo(ActionEvent event) {
		if (packageCombo.getValue() != null) {
			packageInfoArea.clear();
			packageInfoArea.appendText("Paketin ID: \t\t" + packageCombo.getValue().getPackageID() + "\n");
			packageInfoArea.appendText("Toimitusluokka: \t" + packageCombo.getValue().getShippingClass() + "\n");
			packageInfoArea.appendText("Esine: \t\t\t" + packageCombo.getValue().getItemName() + "\n");
			packageInfoArea.appendText("Alkupiste: \t" + packageCombo.getValue().getStartOffice() + "\n");
			packageInfoArea.appendText("Päätepiste: \t" + packageCombo.getValue().getEndOffice());
		}
	}
	
	/*
	 *  Populoi comboBoxin pakettiautomaateilla
	 *  Suodattaa tulokset valitun kaupungin perusteella
	 */
	public void startOfficesForCombos() {
		startOfficeCombo.getItems().clear();
    	for (PostOffice item: ListManager.getPostList().pList) {
    		if (startCityCombo.getValue() != null) {
	    		String city = item.getCity();
	    		if (city.equals(startCityCombo.getValue())) {
	    			startOfficeCombo.getItems().add(item);
	    		} else {
	    			continue;
	    		}
    		} else {
    			startOfficeCombo.getItems().add(item);
    		}
    	}
	}
	
	// Populoi comboBoxin paketeilla
	public void packagesForCombos() {
		packageCombo.getItems().clear();
		for (Package item: ListManager.getPackageList()) {
			packageCombo.getItems().add(item);
		}
	}
	
	// Populoi comboBoxin kaupungeilla
	public void citiesForCombos(ComboBox<String> comboBox) {
		for (PostOffice item: ListManager.getPostList().pList) {
			String city = item.getCity();
			if (comboBox.getItems().contains(city)) {
				continue;
			} else {
				comboBox.getItems().add(city);
			}
		}
	}
	
	@Override
    public void initialize(URL url, ResourceBundle rb) {
		webView.getEngine().load(getClass().getResource("index.html").toExternalForm());
		SqlManager.getPostSelect().selectItem(ListManager.getItemList());
		if (ListManager.getShipmentClassList().size() < 3) {
			SqlManager.getPostSelect().selectClass(ListManager.getShipmentClassList());
		}
		SqlManager.getPostSelect().selectPackage(ListManager.getPackageList(), ListManager.getPackageSentList());
		citiesForCombos(startCityCombo);	
	}
}