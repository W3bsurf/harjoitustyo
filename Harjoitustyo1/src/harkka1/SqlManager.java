package harkka1;

/*
 * Tekijä: Joonas Hakkarainen 0438374
 */

public class SqlManager {
	private static Insert postInsert = new Insert();
	private static Select postSelect = new Select();
	private static Update postUpdate = new Update();
	private static Delete postDelete = new Delete();
	
	
	// Palauttavat tietokannan kanssa kommunikointiin tarvittavat luokat
	public static Insert getPostInsert() {
		return postInsert;
	}
	public static Select getPostSelect() {
		return postSelect;
	}
	public static Update getPostUpdate() {
		return postUpdate;
	}
	public static Delete getPostDelete() {
		return postDelete;
	}
	
}
