package harkka1;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/*
 * Tekijä: Joonas Hakkarainen 0438374
 */

public class Delete {
	
	// Poistaa määritellyn paketin tietokannasta ja lisää tiedon tapahtumasta tietokannan lokiin
	public void deletePackage(int packageID) {
		String sql1 = "INSERT INTO Loki(Tapahtuma) VALUES(?)";
		String sql2 = "INSERT INTO Pakettiloki(Pakettitapahtuma) VALUES(?)";
		String sql3 = "DELETE FROM Paketti"
				+ " WHERE PakettiID = ?";
		ConnectDB connectDB = new ConnectDB();
		Connection conn = null;
		try {
			conn = connectDB.connect();
			if (conn == null) {
				return;
			}
			
			PreparedStatement pstmt1 = conn.prepareStatement(sql1);
			
			pstmt1.setString(1, "Tietokannasta poistettiin paketti ID:" + packageID);
			pstmt1.executeUpdate();
			
			PreparedStatement pstmt2 = conn.prepareStatement(sql2);
			
			pstmt2.setString(1, "Paketti nro: " + packageID + " poistettiin varastosta.");
			pstmt2.executeUpdate();

			PreparedStatement pstmt3 = conn.prepareStatement(sql3);
			
			pstmt3.setInt(1, packageID);
			pstmt3.executeUpdate();
			
			conn.commit();
		} catch(SQLException e1) {
			System.out.println(e1.getLocalizedMessage());
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e2) {
				System.out.println("Yhteyttä ei saatu suljettua");
			}
		}
	}
	
	
	// Poistaa määritellyn esineen tietokannasta ja lisää tiedon tapahtumasta tietokannan lokiin
	public void deleteItem(int itemID) {
		String sql1 = "DELETE FROM Sisältö"
				+ " WHERE SisältöID = ?";
		String sql2 = "INSERT INTO Loki(Tapahtuma) VALUES(?)";
		ConnectDB connectDB = new ConnectDB();
		Connection conn = null;
		try {
			conn = connectDB.connect();
			if (conn == null) {
				return;
			}
			PreparedStatement pstmt1 = conn.prepareStatement(sql1);
			
			pstmt1.setInt(1,  itemID);
			pstmt1.executeUpdate();
			
			PreparedStatement pstmt2 = conn.prepareStatement(sql2);
			
			pstmt2.setString(1, "Tietokannasta poistettiin esine ID:" + itemID);
			pstmt2.executeUpdate();
			
			conn.commit();
		} catch(SQLException e1) {
			System.out.println(e1.getLocalizedMessage());
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e2) {
				System.out.println("Yhteyttä ei saatu suljettua");
			}
		}
	}

}
