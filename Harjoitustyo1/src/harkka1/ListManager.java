package harkka1;

import java.util.ArrayList;

/*
 * Tekijä: Joonas Hakkarainen 0438374
 */

public class ListManager {
	private static POSTList postList = POSTList.getInstance();
	private static ArrayList<Item> itemList = new ArrayList<Item>();
	private static ArrayList<ShipmentClass> shipmentClassList = new ArrayList<ShipmentClass>();
	private static ArrayList<Package> packageList = new ArrayList<Package>();
	private static ArrayList<Delivery> deliveryList = new ArrayList<Delivery>();
	private static ArrayList<Package> packageSentList = new ArrayList<Package>();
	
	public static POSTList getPostList() {
		return postList;
	}
	public static ArrayList<Item> getItemList() {
		return itemList;
	}
	public static ArrayList<ShipmentClass> getShipmentClassList() {
		return shipmentClassList;
	}
	public static ArrayList<Package> getPackageList() {
		return packageList;
	}
	public static ArrayList<Delivery> getDeliveryList() {
		return deliveryList;
	}
	public static ArrayList<Package> getPackageSentList() {
		return packageSentList;
	}
	
}
