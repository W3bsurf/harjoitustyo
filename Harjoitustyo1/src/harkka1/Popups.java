package harkka1;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

/*
 * Tekijä: Joonas Hakkarainen 0438374
 */

public class Popups {
	
	// Luo määritellyn mukaisen varoitus ikkunan
	public static void popup(String title, String headerText, String contentText) {
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle(title);
		alert.setHeaderText(headerText);
		alert.setContentText(contentText);
		alert.showAndWait();
	}
}
