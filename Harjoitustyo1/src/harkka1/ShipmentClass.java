package harkka1;

/*
 * Tekijä: Joonas Hakkarainen 0438374
 */

public class ShipmentClass {
	int shippingClass;
	double sizeRestriction;
	boolean rough;
	int routeRestriction;
	
	public ShipmentClass(int new_shippingClass, double new_sizeRestriction, boolean new_rough, int new_routeRestriction) {
		shippingClass = new_shippingClass;
		sizeRestriction = new_sizeRestriction;
		rough = new_rough;
		routeRestriction = new_routeRestriction;
	}
	
	@Override
	public String toString() {
		return Integer.toString(shippingClass);
	}
	
	public int getShippingClass() {
		return shippingClass;
	}
	
	public double getSizeRestriction() {
		return sizeRestriction;
	}
	
	public boolean getRough() {
		return rough;
	}
	
	public int getRouteRestriction() {
		return routeRestriction;
	}
}
