package harkka1;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/*
 * Tekijä: Joonas Hakkarainen 0438374
 */

public class Update {
	
	// Päivittää paketin ja toimituksen statuksen lähetetyiksi
	public void updatePackageStatus(ArrayList<Package> packageList, ArrayList<Package> packageSentList, int packageID) {
		String sql1 = "UPDATE Paketti SET Toimitettu = 1"
				+ " WHERE PakettiID = ?";
		String sql2 = "UPDATE Toimitus SET Toimitettu = 1"
				+ " WHERE PakettiID = ?";
		String sql3 = "SELECT ToimitusID FROM Toimitus WHERE PakettiID = ?";
		String sql4 = "INSERT INTO Loki(Tapahtuma) VALUES(?)";
		String sql5 = "INSERT INTO Pakettiloki(Pakettitapahtuma) VALUES(?)";
		
		ConnectDB connectDB = new ConnectDB();
		Connection conn = null;
		try {
			conn = connectDB.connect();
			if (conn == null) {
				return;
			}
			PreparedStatement pstmt1 = conn.prepareStatement(sql1);
			
			pstmt1.setInt(1, packageID);
			pstmt1.executeUpdate();
			
			PreparedStatement pstmt2 = conn.prepareStatement(sql2);
			
			pstmt2.setInt(1, packageID);
			pstmt2.executeUpdate();
			
			PreparedStatement pstmt3 = conn.prepareStatement(sql3);
			
			pstmt3.setInt(1, packageID);
			ResultSet rs = pstmt3.executeQuery();
		    int deliveryID = rs.getInt(1);
			
			PreparedStatement pstmt4 = conn.prepareStatement(sql4);
			
			pstmt4.setString(1, "Tietokantaan päivitettiin paketti ID:" + packageID + " lähetetyksi");
			pstmt4.executeUpdate();
			
			PreparedStatement pstmt5 = conn.prepareStatement(sql5);
		    
		    pstmt5.setString(1, "Lähetettiin paketti nro: " + packageID + " toimituksessa nro: " + deliveryID + ".");
		    pstmt5.executeUpdate();
			
			conn.commit();
			for (int i = 0; packageList.size() > i; i++) {
				if (packageList.get(i).getPackageID() == packageID) {
					packageSentList.add(packageList.get(i));
					packageList.remove(i);
				}
			}
		} catch (SQLException e1) {
			System.out.println(e1.getLocalizedMessage());
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e2) {
				System.out.println("Yhteyttä ei saatu suljettua.");
			}
		}
		
	}
}
