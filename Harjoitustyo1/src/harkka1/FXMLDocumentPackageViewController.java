package harkka1;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.fxml.Initializable;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/*
 * Tekijä: Joonas Hakkarainen 0438374
 */

public class FXMLDocumentPackageViewController implements Initializable {
	public WebView webView;
    
	@FXML
    private Label packageViewLabel;

    @FXML
    private Label choosePackagaDeleteLabel;

    @FXML
    private Label chooseItemLabel;
    
    @FXML
    private Label packageCreationLabel;
    
    @FXML
    private Label packageDeleteLabel;
    
    @FXML
    private Label routeLengthLabel;
    
    @FXML
    private Label chooseClassTitle;
    
    @FXML
    private Label classInfoLabel;
    
    @FXML
    private Label startpointLabel;

    @FXML
    private Label endpointLabel;
    
    @FXML
    private Label itemInfoLabel;
    
    @FXML
    private TextField routeLengthField;

    @FXML
    private Button cancelButton;

    @FXML
    private Button createPackageButton;
    
    @FXML
    private Button deletePackageButton;
    
    @FXML
    private ComboBox<String> startCityCombo;

    @FXML
    private ComboBox<PostOffice> startOfficeCombo;

    @FXML
    private ComboBox<String> endCityCombo;

    @FXML
    private ComboBox<PostOffice> endOfficeCombo;

    @FXML
    private ComboBox<Package> packageDeleteCombo;

    @FXML
    private ComboBox<ShipmentClass> shipmentClassCombo;

    @FXML
    private ComboBox<Item> itemCombo;

    @FXML
    private TextArea classInfoArea;
    
    @FXML
    private TextArea itemInfoArea;
    
    // Asettaa nykyisen ikkunan weViewin annetun webView mukaiseksi
    public void setWebView(WebView new_webView) {
    	webView = new_webView;
    }
    
    // Sulkee nykyisen ikkunan
    public void cancel(ActionEvent event) {
		Stage cStage = (Stage)((Node)event.getSource()).getScene().getWindow();
		cStage.close();
	}
    
    // Asettaa textFieldiin valittujen pakettiautomaattien välisen etäisyyden
    public void routeLengthEvent(ActionEvent event) {
    	if (startOfficeCombo.getValue() != null &&  endOfficeCombo.getValue() != null) {
    		ArrayList<String> coordinateList = SqlManager.getPostSelect().selectCoordinates(startOfficeCombo.getValue().getOffice(), endOfficeCombo.getValue().getOffice());
    		String routeString =  String.valueOf(webView.getEngine().executeScript("document.calcLength(" + coordinateList + ")"));
    		routeLengthField.setText(routeString + " km");
    	}
    }
    
    // Luo annettujen tietojen perusteella paketin ja toimituksen tietokantaan ja listaan
	public void createPackage(ActionEvent event) {
		if (!itemCombo.getSelectionModel().isEmpty() && !shipmentClassCombo.getSelectionModel().isEmpty() && !startOfficeCombo.getSelectionModel().isEmpty() && !endOfficeCombo.getSelectionModel().isEmpty()) {
			ArrayList<String> coordinateList = SqlManager.getPostSelect().selectCoordinates(startOfficeCombo.getValue().getOffice(), endOfficeCombo.getValue().getOffice());
			String routeString =  String.valueOf(webView.getEngine().executeScript("document.calcLength(" + coordinateList + ")"));
			Double routeLength = Double.parseDouble(routeString);
			int itemID;
			
			if (itemCombo.getValue().getSize() < shipmentClassCombo.getValue().getSizeRestriction() && routeLength <= shipmentClassCombo.getValue().getRouteRestriction()) {
				itemID = SqlManager.getPostInsert().insertItemSent(itemCombo.getValue().getName(), itemCombo.getValue().getSize(), itemCombo.getValue().getBreakable(), itemCombo.getValue().getWeight());
				SqlManager.getPostInsert().insertPackage(ListManager.getPackageList(), shipmentClassCombo.getValue().getShippingClass(), itemID, routeLength, itemCombo.getValue().getName(), startOfficeCombo.getValue().getOffice(), endOfficeCombo.getValue().getOffice());
				
				packageCreationLabel.setText(itemCombo.getValue().getName() + " lisätty pakettivarastoon.");
			} else if (itemCombo.getValue().getSize() > shipmentClassCombo.getValue().getSizeRestriction() && routeLength <= shipmentClassCombo.getValue().getRouteRestriction()) {
				Popups.popup("Huomio", "Paketin luominen ei onnistunut!", "Esine on liian suuri valittuun\npaketin toimitusluokkaan.");
			} else if (itemCombo.getValue().getSize() < shipmentClassCombo.getValue().getSizeRestriction() && routeLength > shipmentClassCombo.getValue().getRouteRestriction()) {
				Popups.popup("Huomio", "Paketin luominen ei onnistunut!", "Matka on liian pitkä valittuun\npaketin toimitusluokkaan.");
			}
		} else {
			Popups.popup("Huomio", "Paketin luominen ei onnistunut!", "Kaikkia vaadittuja arvoja ei ole syötetty.");
		}
	}
	
	// Poistaa comboBoxissa valittuna olevan paketin tietokannasta ja listasta
	public void deletePackage(ActionEvent event) {
		if (packageDeleteCombo.getValue() != null) {
			SqlManager.getPostDelete().deletePackage(packageDeleteCombo.getValue().getPackageID());
			packageDeleteLabel.setText(packageDeleteCombo.getValue().getItemName() + " poistettu pakettivarastosta.");
			packageDeleteCombo.getSelectionModel().clearSelection();
			SqlManager.getPostSelect().selectPackage(ListManager.getPackageList(), ListManager.getPackageSentList());
		} else {
			Popups.popup("Huomio", "Paketin poistaminen ei onnistunut!", "Valitse ensin poistettava paketti.");
		}
	}
	
	// Tulostaa valitun toimitusluokan tiedot textAreaan
	public void printClassInfo(ActionEvent event) {
		classInfoArea.clear();
		classInfoArea.appendText("Toimitusluokka: \t" + shipmentClassCombo.getValue().getShippingClass() + "\n");
		classInfoArea.appendText("Kokorajoitus: \t\t" + shipmentClassCombo.getValue().getSizeRestriction() + " litraa\n");
		classInfoArea.appendText("Matkarajoitus: \t" + shipmentClassCombo.getValue().getRouteRestriction() + " kilometriä\n");
		
		if (shipmentClassCombo.getValue().getRough() == true) {
			classInfoArea.appendText("Ei suositella särkyville esineille.");
		} else {
			classInfoArea.appendText("Toimitusluokka sopii särkyville esineille.");
		}
	}
	
	// Tulostaa valitun esineen tiedot textAreaan
	public void printItemInfo(ActionEvent event) {
		if (itemCombo.getValue() != null) {
			itemInfoArea.clear();
			itemInfoArea.appendText("Esine: \t" + itemCombo.getValue().getName() + "\n");
			itemInfoArea.appendText("Koko: \t" + itemCombo.getValue().getSize() + " litraa\n");
			itemInfoArea.appendText("Paino: \t" + itemCombo.getValue().getWeight() + " kiloa\n");
			
			if (itemCombo.getValue().getBreakable() == 1) {
				itemInfoArea.appendText("Särkyvä");
			} else {
				itemInfoArea.appendText("Ei särkyvä");
			}
		}
	}
    
	// Populoi comboBoxin esineillä
    public void itemsForCombos() {
		itemCombo.getItems().clear();
		for (Item item: ListManager.getItemList()) {
			itemCombo.getItems().add(item);
		}
	}
	
    // Populoi comboBoxin toimitusluokilla
	public void classesForCombos() {
		shipmentClassCombo.getItems().clear();
		for (ShipmentClass item : ListManager.getShipmentClassList()) {
			shipmentClassCombo.getItems().add(item);
		}
	}
	
	// Populoi comboBoxin pakettiautomaateilla
	public void startOfficesForCombos() {
		startOfficeCombo.getItems().clear();
    	for (PostOffice item: ListManager.getPostList().pList) {
    		if (startCityCombo.getValue() != null) {
	    		String city = item.getCity();
	    		if (city.equals(startCityCombo.getValue())) {
	    			startOfficeCombo.getItems().add(item);
	    		} else {
	    			continue;
	    		}
    		} else {
    			startOfficeCombo.getItems().add(item);
    		}
    	}
	}
	
	// Populoi comboBoxin pakettiautomaateilla
	public void endOfficesForCombos() {
		endOfficeCombo.getItems().clear();
    	for (PostOffice item: ListManager.getPostList().pList) {
    		if (endCityCombo.getValue() != null) {
    			String city = item.getCity();
	    		if (city.equals(endCityCombo.getValue())) {
	    			endOfficeCombo.getItems().add(item);
	    		} else {
	    			continue;
	    		}
    		} else {
    			endOfficeCombo.getItems().add(item);
    		}
    	}
	}
	
	// Populoi comboBoxin paketeilla
	public void packagesForCombos() {
		packageDeleteCombo.getItems().clear();
		for (Package item: ListManager.getPackageList()) {
			packageDeleteCombo.getItems().add(item);
		}
	}
	
	// Populoi annetun comboBoxin kaupungeilla
	public void citiesForCombos(ComboBox<String> comboBox) {
		for (PostOffice item: ListManager.getPostList().pList) {
			String city = item.getCity();
			if (comboBox.getItems().contains(city)) {
				continue;
			} else {
				comboBox.getItems().add(city);
			}
		}
	}
	
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		citiesForCombos(startCityCombo);
		citiesForCombos(endCityCombo);		
		classesForCombos();
	}
}
